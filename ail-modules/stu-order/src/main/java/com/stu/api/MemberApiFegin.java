package com.stu.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name="stu-member")
public interface MemberApiFegin {
	@GetMapping("/getMember")
	public String getMember();

}
