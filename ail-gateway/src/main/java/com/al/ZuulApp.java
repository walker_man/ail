package com.al;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulServer;

@EnableZuulServer
@SpringBootApplication
public class ZuulApp {
		public static void main(String[] args) {
			SpringApplication.run(ZuulApp.class, args);
		}
}
