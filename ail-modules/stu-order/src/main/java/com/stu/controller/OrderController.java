package com.stu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stu.api.MemberApiFegin;

@RestController
public class OrderController {
	@Autowired
	private MemberApiFegin memberApi;
	
	@GetMapping("/feginTest")
	public String feginTest() {
		return memberApi.getMember();
	}
}
