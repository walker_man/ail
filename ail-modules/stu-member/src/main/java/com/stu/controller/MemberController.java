package com.stu.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class MemberController {
	@Value("${testname}")
	private String testName;	//gitee中配置文件的属性
	
	@Value("${server.port}")
	private String port;
	/**
	 * configclient测试 读取gitee配置中的属性
	 * @return
	 */
	@GetMapping("ccTest")
	public String configClientTest() {
		return testName;
	}
	@GetMapping("getMember")
	public String getMember() {
		return "这是member服务，端口号是"+port;
	}
}
